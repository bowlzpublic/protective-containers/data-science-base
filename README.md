# Data Science Base

The hardest part of Data Science is effectively setting up your workspace.  The steps are manual and often are difficult to replicate across members of a team.  If only there was some kind of free app that could bundle data science into a box and deliver it to you...  or even a computer with pre-installed packages... and when the packages get updated then everyone gets a new app or computer.

Well you can essentially achieve that using Docker and VSCode

## Running through VSCode:  

### Git

First thing is first you need to install Git Bash for Windows on your machine

#### Steps
- Download and Install [Git Bash](https://www.git-scm.com/download/win)
- Launch Git Bash and set your global config
- Configs
```
# Replace 'John Doe' and 'johndoe@example.com' with your personal information
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### Remote Server

To avoid having each Data Scientist setting up python, docker and things that make the development straight forward, the recommendation is to use a remote server with the CPU and RAM needed.  (And even a GPU if needed)

On the Remote server, you will need to setup SSH connectivity.
#### STEPS
- Generate a SSH Key:  
  - Repeat this for each home directory you have:  Y: and C:
  - Run the command: `ssh-keygen`
  - Accepted the default path by just hitting enter: `Enter file in which to save the key (C:\Users\<username>/.ssh/id_rsa):`
  - Leave the passphrase blank by just hitting enter (2x), twice
  - You should see the following output
  ```
  Your identification has been saved in C:\Users\<username/.ssh/id_rsa.
  Your public key has been saved in C:\Users\<username>/.ssh/id_rsa.pub.
  The key fingerprint is:
  SHA256:wGXMPFt2H+k0bkEC6RaJ/fJ1VYH8Bsi8MdjWRPCg85U ntdomain\<username>@VDIW10D1473
  The key's randomart image is:
  +---[RSA 3072]----+
  |       +oo**O+oo+|
  |     . o*.OO+*B .|
  |      o  O.++Eo+.|
  |       .. *.o *o.|
  |        S. + o.. |
  |            .    |
  |                 |
  |                 |
  |                 |
  +----[SHA256]-----+
```
``````
- copy the SSH key to the server
  - Run the following command: `cat id_rsa.pub | ssh <username>@<servername> "cat >> ~/.ssh/authorized_keys"`
  - You should be prompted for your password (enter that and continue)
  - Now SSH into the machine
  - You should no longer be prompted for a password, however if you are do the following:
    - After entering your password you should have a prompt
    - Execute this command (assuming linux box): `chmod 700 .ssh`
    - Then Execute this command: `chmod 640 .ssh/authorized_keys`
    - Exit the machine, now try to SSH in and it should work.
- Now login to the server 1 more time and setup your git config
```
# Replace 'John Doe' and 'johndoe@example.com' with your personal information
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

By loading your ssh key onto the server, the server getting 2 things a key that validates who you are and where your coming from.  The underlying assumption is that you had access to that machine you came from and you have access to the server, the key just facilitates a trusted SSO process

#### STEPS
- Download and Install VSCode
- Install the Remote Extensions:  
  - [SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)
  - [Dev Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
- Now open the command palette: `cntl + shift + P`
- Then find and run "Connect to Host.." ![image.png](./command_palette.png)
- If this is the first time doing this from VSCode... then enter the username and server info:  `<username>@<servername>`
- This will then Remote into the host and use the Server through VSCode and allow all commands to execute on the server
- Now navigate to your code folder through File --> Open Folder.. and open your project folder.  If you don't have a project yet, you can now `git clone` a project
- VSCode should now reload the window for this project folder.
- If you don't already have a `.devcontainer` folder, create the folder and copy the contents of [this](./devcontainer) to that folder
- This should prompt you to re-open the folder in a dev container... do this
- After you do this, the server will load up the environment and now create your own personalized conda workspace for your project

#### If you have Podman vs. Docker
[VS Code & Podman](https://code.visualstudio.com/remote/advancedcontainers/docker-options#_podman)

Podman works for the most part like Docker and before you start need to change these settings
